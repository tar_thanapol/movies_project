package th.co.digio.movies.project.implement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.co.digio.movies.project.StaticClass;
import th.co.digio.movies.project.dto.MovieDTO;
import th.co.digio.movies.project.dto.MoviesDTO;
import th.co.digio.movies.project.dto.ResponseDataDTO;
import th.co.digio.movies.project.entity.Movie;
import th.co.digio.movies.project.entity.MovieCastRef;
import th.co.digio.movies.project.entity.MovieGenresRef;
import th.co.digio.movies.project.enumuration.CommonMovieStatus;
import th.co.digio.movies.project.repository.MovieCastRefRepository;
import th.co.digio.movies.project.repository.MovieGenresRefRepository;
import th.co.digio.movies.project.repository.MovieRepository;
import th.co.digio.movies.project.service.MovieService;
import th.co.digio.movies.project.util.SetResponse;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.logging.Logger;

import static th.co.digio.movies.project.StaticClass.findMovie;

@Component
public class MovieServiceImplement implements MovieService {
    private final Logger logger = Logger.getLogger(String.valueOf(MovieServiceImplement.class));
    public List<MoviesDTO> movies = new ArrayList<>();
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private MovieCastRefRepository movieCastRefRepository;
    @Autowired
    private MovieGenresRefRepository movieGenresRefRepository;

    @Override
    public ResponseDataDTO searchPartial(String text) {
        text = text.trim();
        Instant begin = Instant.now();
        logger.warning(String.valueOf(begin) + " is time start");
        List<Movie> movie = movieRepository.findByTitleLike(text);
        logger.warning(String.valueOf((Duration.between(begin, Instant.now()).toMillis()) + " is time execute/millisecond"));
        if (movie == null || movie.size() == 0) {
            return SetResponse.setResponseResult(CommonMovieStatus.INVALID_MOVIE_TITLE, null);
        }
        return SetResponse.setResponseResult(CommonMovieStatus.SUCCESS, movie);
    }

    @Override
    public ResponseDataDTO searchFullText(String text) {
        text = text.trim();
        Instant begin = Instant.now();
        logger.warning(String.valueOf(begin) + " is time start");
//        List<Movie> movie = customMovieRepoImplement.search(text);
        List<Movie> movie = movieRepository.findByTitle(text);
        logger.warning(String.valueOf((Duration.between(begin, Instant.now()).toMillis())) + " is time execute/millisecond " + movie.size());
        if (movie == null || movie.size() == 0) {
            return SetResponse.setResponseResult(CommonMovieStatus.INVALID_MOVIE_TITLE, null);
        }
        return SetResponse.setResponseResult(CommonMovieStatus.SUCCESS, movie);
    }

    @Override
    public ResponseDataDTO searchInvertIndex(String text) {
        HashMap<String, MoviesDTO> movieFromSearch = new HashMap<>();
        text = text.trim();

        Instant begin = Instant.now();
        String[] splWord = text.split(" ");
        for (String word : splWord) {
            word.replaceAll("[^a-zA-Z0-9\\s]", "");
            movieFromSearch.putAll(findMovie(word, movieFromSearch));
        }

        logger.warning(String.valueOf(begin) + " is time start search" + " : " + movieFromSearch.size());
        logger.warning(String.valueOf((Duration.between(begin, Instant.now()).toMillis())) + " is time execute/millisecond ");
        List<MoviesDTO> moviesDTOS = new ArrayList<>(movieFromSearch.values());
        if (moviesDTOS == null || moviesDTOS.size() == 0) {
            return SetResponse.setResponseResult(CommonMovieStatus.INVALID_MOVIE_TITLE, null);
        }
        return SetResponse.setResponseResult(CommonMovieStatus.SUCCESS, moviesDTOS);
    }

    @Override
    public ResponseDataDTO searchInvertIndexDB(String text) {
        HashMap<Long, Movie> movieFromSearch = new HashMap<>();
        text = text.trim();

        Instant begin = Instant.now();
        String[] splWord = text.split(" ");
        for (String word : splWord) {
            word.replaceAll("[^a-zA-Z0-9\\s]", "");
            movieFromSearch.putAll(findMovieFromDB(word, movieFromSearch));
        }

        logger.warning(String.valueOf(begin) + " is time start search" + " : " + movieFromSearch.size());
        logger.warning(String.valueOf((Duration.between(begin, Instant.now()).toMillis())) + " is time execute/millisecond ");
        List<MovieDTO> moviesDTOS = new ArrayList<>();
        for (Map.Entry<Long, Movie> entry : movieFromSearch.entrySet()) {
            Movie movie = movieRepository.findById(entry.getKey()).get();
            List<MovieGenresRef> movieGenresRefs = movieGenresRefRepository.findByMovie_Id(entry.getKey());
            List<MovieCastRef> movieCastRefs = movieCastRefRepository.findByMovie_Id(entry.getKey());
            moviesDTOS.add(new MovieDTO(movie.getTitle(), movie.getYear(), movieCastRefs, movieGenresRefs));
        }
        if (moviesDTOS == null || moviesDTOS.size() == 0) {
            return SetResponse.setResponseResult(CommonMovieStatus.INVALID_MOVIE_TITLE, null);
        }
        return SetResponse.setResponseResult(CommonMovieStatus.SUCCESS, moviesDTOS);
    }

    public HashMap<Long, Movie> findMovieFromDB(String text, HashMap<Long, Movie> movieFromSearch) {

        text = text.toLowerCase(Locale.ROOT);
        HashMap<Long, Movie> movieFromSearchShow = new HashMap<>();
        if (movieFromSearch.size() == 0 || movieFromSearch == null) {
            movieFromSearchShow.putAll(StaticClass.moviesObjectDB);
        } else {
            movieFromSearchShow.putAll(movieFromSearch);
            movieFromSearch.clear();
        }
        if (StaticClass.moviesDocumentDB.containsKey(text)) {
            for (Long id : StaticClass.moviesDocumentDB.get(text)) {
                if (movieFromSearchShow.containsKey(id)) {
                    movieFromSearch.put(id, StaticClass.moviesObjectDB.get(id));
                }
            }
        }
        return movieFromSearch;
    }

}

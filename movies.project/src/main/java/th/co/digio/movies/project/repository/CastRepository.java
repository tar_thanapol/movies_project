package th.co.digio.movies.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import th.co.digio.movies.project.entity.Cast;

@Repository
public interface CastRepository extends JpaRepository<Cast, Long> {
    Cast findByName(String name);
    boolean existsByName(String name);
}

package th.co.digio.movies.project.entity;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import javax.persistence.*;

@Entity
@Table(name = "MOVIE_GENRES_REF")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MovieGenresRef {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Movie movie;
    @ManyToOne
    private Genres genres;

    public MovieGenresRef(Movie movie, Genres genres) {
        this.movie = movie;
        this.genres = genres;
    }

    public MovieGenresRef() {

    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Genres getGenres() {
        return genres;
    }

    public void setGenres(Genres genres) {
        this.genres = genres;
    }
}

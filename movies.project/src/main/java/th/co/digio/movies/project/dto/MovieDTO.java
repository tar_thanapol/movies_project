package th.co.digio.movies.project.dto;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import th.co.digio.movies.project.entity.MovieCastRef;
import th.co.digio.movies.project.entity.MovieGenresRef;

import java.util.List;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MovieDTO {
    private String title;
    private String year;
    private List<MovieCastRef> cast;
    private List<MovieGenresRef> genres;

    public MovieDTO() {
    }

    public MovieDTO(String title, String year, List<MovieCastRef> cast, List<MovieGenresRef> genres) {
        this.title = title;
        this.year = year;
        this.cast = cast;
        this.genres = genres;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public List<MovieCastRef> getCast() {
        return cast;
    }

    public void setCast(List<MovieCastRef> cast) {
        this.cast = cast;
    }

    public List<MovieGenresRef> getGenres() {
        return genres;
    }

    public void setGenres(List<MovieGenresRef> genres) {
        this.genres = genres;
    }

    @Override
    public String toString() {
        return "MoviesDTO{" +
                "title='" + title + '\'' +
                ", year='" + year + '\'' +
                ", cast=" + cast +
                ", genres=" + genres +
                '}';
    }
}

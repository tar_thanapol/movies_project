package th.co.digio.movies.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import th.co.digio.movies.project.entity.Genres;

@Repository
public interface GenresRepository extends JpaRepository<Genres, Long> {
    Genres findByType(String type);
    boolean existsByType(String type);
}

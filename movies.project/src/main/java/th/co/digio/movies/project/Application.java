package th.co.digio.movies.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import th.co.digio.movies.project.dto.MoviesDTO;
import th.co.digio.movies.project.entity.Movie;
import th.co.digio.movies.project.entity.MovieCastRef;
import th.co.digio.movies.project.entity.MovieGenresRef;
import th.co.digio.movies.project.implement.MovieServiceImplement;
import th.co.digio.movies.project.repository.MovieCastRefRepository;
import th.co.digio.movies.project.repository.MovieGenresRefRepository;
import th.co.digio.movies.project.repository.MovieRepository;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

@EnableJpaRepositories(basePackages = "th.co.digio.movies.project.repository")
@EntityScan(basePackages = "th.co.digio.movies.project.entity")
@ComponentScan(basePackages = {"th.co.digio.movies.project"})
@SpringBootApplication
@EnableScheduling
public class Application implements CommandLineRunner {
    private final Logger logger = Logger.getLogger(String.valueOf(Application.class));
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private MovieGenresRefRepository movieGenresRefRepository;
    @Autowired
    private MovieCastRefRepository movieCastRefRepository;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

    }

    @Override
    public void run(String... args) throws Exception {
        Instant begin = Instant.now();
        logger.warning(String.valueOf(begin) + " is time start");
        List<Movie> movies = movieRepository.findAll();
        movies.forEach((movie -> {
            String[] splWord = movie.getTitle().toLowerCase(Locale.ROOT).split(" ");
            for (String word : splWord) {
                word = word.replaceAll("[^a-zA-Z0-9\\s]", "");
                if (StaticClass.moviesDocumentDB.get(word) == null) {
                    List<Long> listValue = new ArrayList<>();
                    listValue.add(movie.getId());
                    List<String> stringList = new ArrayList<>();
                    stringList.add(String.valueOf(movie.getId()));
                    StaticClass.moviesDocument.put(word, stringList);
                    StaticClass.moviesDocumentDB.put(word, listValue);

                } else {
                    List<Long> listValue = StaticClass.moviesDocumentDB.get(word);
                    List<String> stringList = StaticClass.moviesDocument.get(word);
                    stringList.add(String.valueOf(movie.getId()));
                    StaticClass.moviesDocument.put(word, stringList);
                    listValue.add(movie.getId());
                    StaticClass.moviesDocumentDB.replace(word, listValue);
                }
            }
            StaticClass.moviesObjectDB.put(movie.getId(), movie);
        }));
        logger.warning(String.valueOf((Duration.between(begin, Instant.now()).toMillis()) + " is time execute/millisecond"));
        logger.info("Movie is " + StaticClass.moviesObjectDB.size() + " Doc is " + StaticClass.moviesDocumentDB.size());
    }
}

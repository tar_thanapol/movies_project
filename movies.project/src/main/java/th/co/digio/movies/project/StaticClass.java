package th.co.digio.movies.project;

import org.springframework.beans.factory.annotation.Autowired;
import th.co.digio.movies.project.dto.MoviesDTO;
import th.co.digio.movies.project.entity.Movie;
import th.co.digio.movies.project.entity.MovieCastRef;
import th.co.digio.movies.project.repository.MovieRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class StaticClass {

    public static HashMap<String, List<String>> moviesDocument = new HashMap<String, List<String>>();
    public static HashMap<String, MoviesDTO> moviesObject = new HashMap<String, th.co.digio.movies.project.dto.MoviesDTO>();

    public static HashMap<String, List<Long>> moviesDocumentDB = new HashMap<String, List<Long>>();
    public static HashMap<Long, Movie> moviesObjectDB = new HashMap<Long, Movie>();

    public static HashMap<String, MoviesDTO> findMovie(String txt, HashMap<String, MoviesDTO> movieFromSearch) {
        txt = txt.toLowerCase(Locale.ROOT);
        HashMap<String, MoviesDTO> movieFromSearchShow = new HashMap<>();
        if (movieFromSearch.size() == 0 || movieFromSearch == null) {
            movieFromSearchShow.putAll(StaticClass.moviesObject);
        } else {
            movieFromSearchShow.putAll(movieFromSearch);
            movieFromSearch.clear();
        }
        int i = 0;
        if (StaticClass.moviesDocument.containsKey(txt)) {
            for (String title : StaticClass.moviesDocument.get(txt)) {
                if (movieFromSearchShow.containsKey(title)) {
                    movieFromSearch.put(title, StaticClass.moviesObject.get(title));
                }
            }
        }
        return movieFromSearch;
    }


    public static String showStr(List<String> longs) {
        String str = "";
        for (String aLong : longs) {
            str = str + " | " + aLong.toString();
        }
        return str;
    }
}

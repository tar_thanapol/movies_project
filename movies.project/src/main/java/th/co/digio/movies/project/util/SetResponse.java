package th.co.digio.movies.project.util;

import org.springframework.stereotype.Component;
import th.co.digio.movies.project.dto.ResponseDataDTO;
import th.co.digio.movies.project.enumuration.CommonMovieStatus;

@Component
public class SetResponse {
    public static ResponseDataDTO setResponseResult(CommonMovieStatus movieStatus, Object obj){
        ResponseDataDTO responseDataDTO = new ResponseDataDTO();
        responseDataDTO.setCode(movieStatus.getCode());
        responseDataDTO.setMessage(movieStatus.getMessage());
        responseDataDTO.setData(obj);
        return responseDataDTO;
    }
}

package th.co.digio.movies.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import th.co.digio.movies.project.entity.Cast;
import th.co.digio.movies.project.entity.Movie;
import th.co.digio.movies.project.entity.MovieCastRef;

import java.util.List;

@Repository
public interface MovieCastRefRepository extends JpaRepository<MovieCastRef, Long> {
    MovieCastRef findByMovieAndCast(Movie movie, Cast cast);
    List<MovieCastRef> findByMovie_Id(Long aLong);
}

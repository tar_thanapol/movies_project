package th.co.digio.movies.project.schedule.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import th.co.digio.movies.project.StaticClass;
import th.co.digio.movies.project.dto.MoviesDTO;
import th.co.digio.movies.project.entity.*;
import th.co.digio.movies.project.repository.*;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.logging.Logger;

@Component
public class ScheduledTasks {
    private static final Logger logger = Logger.getLogger(String.valueOf(ScheduledTasks.class));
    public String base_url = "https://raw.githubusercontent.com/prust/wikipedia-movie-data/master/movies.json";
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private MovieGenresRefRepository movieGenresRefRepository;
    @Autowired
    private MovieCastRefRepository movieCastRefRepository;
    @Autowired
    private CastRepository castRepository;
    @Autowired
    private GenresRepository genresRepository;

    @Async
    @Scheduled(cron = "0 0 8 * * *") //every 8:00 every day
    public void readDataFormApi() throws JsonProcessingException {
        logger.info("The time is now for read data " + dateFormat.format(new Date()));
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        String jsonString = restTemplate.exchange(base_url, HttpMethod.GET, entity, String.class).getBody();
        ObjectMapper mapper = new ObjectMapper();
        List<MoviesDTO> moviesDTOS = mapper.readValue(jsonString, new TypeReference<List<MoviesDTO>>() {
        });
        logger.info("Movies length : " + moviesDTOS.size());
        Instant begin = Instant.now();
        logger.warning(String.valueOf(begin) + " is time start");
        List<Movie> movies = new ArrayList<>();
        for (int i = 0; i < moviesDTOS.size(); i++) {
            MoviesDTO moviesDTO = moviesDTOS.get(i);
//            logger.info("this is movie " + (i + 1) + " " + moviesDTO.getTitle() + " " + moviesDTO.getYear());
            List<Genres> genresList = null;
            Movie movieAdd = new Movie();
            if (!movieRepository.existsByTitleAndYear(moviesDTO.getTitle(), moviesDTO.getYear())) {
                movieAdd.setTitle(moviesDTO.getTitle());
                movieAdd.setYear(moviesDTO.getYear());
                movies.add(movieAdd);
                movieRepository.save(movieAdd);
                if (moviesDTO.getGenres().size() != 0) {
                    for (String obj : moviesDTO.getGenres()) {
                        Genres genresRes = null;
                        obj = obj.trim();
                        obj = obj.toLowerCase();
//                        Genres genres = genresRepository.existsByType(obj);
                        if (!genresRepository.existsByType(obj)) {
                            Genres genresAdd = new Genres();
                            genresAdd.setType(obj);
                            genresRepository.save(genresAdd);
                            movieGenresRefRepository.save(new MovieGenresRef(movieAdd, genresAdd));
                        }
//                        if (genresRes != null) {
//                            MovieGenresRef movieGenresRef = movieGenresRefRepository.findByMovieAndGenres(movie, genresRes);
//                            if (movieGenresRef == null) {
//                                movieGenresRefRepository.saveAndFlush(new MovieGenresRef(movie, genresRes));
//                            }
//                        } else if (genres != null) {
//                            MovieGenresRef movieGenresRef = movieGenresRefRepository.findByMovieAndGenres(movie, genres);
//                            if (movieGenresRef == null) {
//                                movieGenresRefRepository.saveAndFlush(new MovieGenresRef(movie, genres));
//                            }
//                        }
                    }
                }
                if (moviesDTO.getCast().size() != 0) {
                    for (String obj : moviesDTO.getCast()) {
                        Cast castRes = null;
                        obj = obj.trim();
                        obj = obj.toLowerCase();
//                        Cast cast = castRepository.existsByName(obj);
                        if (!castRepository.existsByName(obj)) {
                            Cast castAdd = new Cast();
                            castAdd.setName(obj);
                            castRepository.save(castAdd);
                            movieCastRefRepository.save(new MovieCastRef(movieAdd, castAdd));
                        }
//                        if (castRes != null) {
//                            MovieCastRef movieCastRef = movieCastRefRepository.findByMovieAndCast(movie, castRes);
//                            if (movieCastRef == null) {
//                                movieCastRefRepository.saveAndFlush(new MovieCastRef(movie, castRes));
//                            }
//                        } else if (cast != null) {
//                            MovieCastRef movieCastRef = movieCastRefRepository.findByMovieAndCast(movie, cast);
//                            if (movieCastRef == null) {
//                                movieCastRefRepository.saveAndFlush(new MovieCastRef(movie, cast));
//                            }
//                        }
                    }
                }
            }

        }
        Instant end = Instant.now();
        logger.warning(String.valueOf(Duration.between(begin, end).toMillis() / 1000) + " is time execute");
    }

    @Async
    @Scheduled(cron = "0 0 8 * * *") //every 8:00 every day
    public void readDataToObject() throws JsonProcessingException {
        logger.info("The time is now for read data " + dateFormat.format(new Date()));
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        String jsonString = restTemplate.exchange(base_url, HttpMethod.GET, entity, String.class).getBody();
        ObjectMapper mapper = new ObjectMapper();
        List<MoviesDTO> moviesDTOS = mapper.readValue(jsonString, new TypeReference<List<MoviesDTO>>() {
        });
        logger.info("Movies length : " + moviesDTOS.size());
        Instant begin = Instant.now();
        logger.warning(String.valueOf(begin) + " is time start");
        for (int i = 0; i < moviesDTOS.size(); i++) {
            String indexValue = moviesDTOS.get(i).getTitle().toLowerCase(Locale.ROOT) + " " + moviesDTOS.get(i).getYear();
            boolean check = StaticClass.moviesObject.containsKey(indexValue);
            if (check) {
                continue;
            }
            MoviesDTO movie = new MoviesDTO();
            movie.setTitle(moviesDTOS.get(i).getTitle());
            movie.setYear(moviesDTOS.get(i).getYear());
            if (moviesDTOS.get(i).getGenres().size() != 0) {
                List<String> listGen = new ArrayList<>();
                for (String obj : moviesDTOS.get(i).getGenres()) {
                    obj = obj.trim().toLowerCase(Locale.ROOT);
                    listGen.add(obj);
                }
                movie.setGenres(listGen);
            }
            if (moviesDTOS.get(i).getCast().size() != 0) {
                List<String> listCast = new ArrayList<>();
                for (String obj : moviesDTOS.get(i).getCast()) {
                    obj = obj.trim().toLowerCase(Locale.ROOT);
                    listCast.add(obj);
                }
                movie.setCast(listCast);
            }
            StaticClass.moviesObject.put(indexValue, movie);
            String[] splWord = moviesDTOS.get(i).getTitle().toLowerCase(Locale.ROOT).split(" ");
            for (String word : splWord) {
                word = word.replaceAll("[^a-zA-Z0-9\\s]", "");
                if (StaticClass.moviesDocument.get(word) == null) {
                    List<String> listValue = new ArrayList<>();
                    listValue.add(indexValue);
                    StaticClass.moviesDocument.put(word, listValue);
                } else {
                    List<String> listValue = StaticClass.moviesDocument.get(word);
                    listValue.add(indexValue);
                    StaticClass.moviesDocument.replace(word, listValue);
                }
            }

        }
        Instant end = Instant.now();
        logger.warning(String.valueOf(Duration.between(begin, end).toMillis() / 1000) + " is time execute " + StaticClass.moviesDocument.size() + " doc : " + StaticClass.moviesObject.size());
    }
}

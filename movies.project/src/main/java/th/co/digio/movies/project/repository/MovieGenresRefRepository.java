package th.co.digio.movies.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import th.co.digio.movies.project.entity.Genres;
import th.co.digio.movies.project.entity.Movie;
import th.co.digio.movies.project.entity.MovieGenresRef;

import java.util.List;

@Repository
public interface MovieGenresRefRepository extends JpaRepository<MovieGenresRef, Long> {
    MovieGenresRef findByMovieAndGenres(Movie movie, Genres genres);
    List<MovieGenresRef> findByMovie_Id(Long aLong);
}

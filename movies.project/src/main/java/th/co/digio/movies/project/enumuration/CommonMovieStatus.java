package th.co.digio.movies.project.enumuration;

public enum CommonMovieStatus {
    SUCCESS("0000", "Success"),
    INVALID_MOVIE_TITLE("1000","Invalid movie title");

    private String code;
    private String message;

    CommonMovieStatus(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

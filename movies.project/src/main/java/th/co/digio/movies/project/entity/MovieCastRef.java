package th.co.digio.movies.project.entity;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import javax.persistence.*;

@Entity
@Table(name = "MOVIE_CAST_REF")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MovieCastRef {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Movie movie;
    @ManyToOne
    private Cast cast;

    public MovieCastRef(Movie movie, Cast cast) {
        this.movie = movie;
        this.cast = cast;
    }

    public MovieCastRef() {

    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Cast getCast() {
        return cast;
    }

    public void setCast(Cast cast) {
        this.cast = cast;
    }
}

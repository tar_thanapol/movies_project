package th.co.digio.movies.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import th.co.digio.movies.project.entity.Movie;

import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
    Movie findByTitleAndYear(String title, String year);
    List<Movie> findByTitleLike(String title);
    List<Movie> findByTitle(String title);
    List<Movie> findAll();
    boolean existsByTitleAndYear(String title, String year);
//    List<Movie> findByTitleAndYear(String string, String year)
}

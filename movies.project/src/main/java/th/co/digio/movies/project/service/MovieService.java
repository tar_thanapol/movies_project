package th.co.digio.movies.project.service;

import org.springframework.stereotype.Service;
import th.co.digio.movies.project.dto.ResponseDataDTO;

@Service
public interface MovieService {
    ResponseDataDTO searchPartial(String string);
    ResponseDataDTO searchFullText(String text);
    ResponseDataDTO searchInvertIndex(String text);
    ResponseDataDTO searchInvertIndexDB(String text);
}

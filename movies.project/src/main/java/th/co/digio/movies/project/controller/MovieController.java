package th.co.digio.movies.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import th.co.digio.movies.project.dto.ResponseDataDTO;
import th.co.digio.movies.project.implement.MovieServiceImplement;

import java.util.logging.Logger;

@RestController
@CrossOrigin("*")
public class MovieController {
    private final Logger logger = Logger.getLogger(String.valueOf(MovieController.class));

    @Autowired
    private MovieServiceImplement movieServiceImplement;

    @RequestMapping(value = "searchPartial", produces = {"application/json"}, method = RequestMethod.POST)
    public ResponseDataDTO searchPartial(@RequestBody String text) {
        logger.info("validate provice : " + text);

        ResponseDataDTO res = new ResponseDataDTO();
        res = movieServiceImplement.searchPartial(text);
        return res;
    }

    @RequestMapping(value = "searchFull", produces = {"application/json"}, method = RequestMethod.POST)
    public ResponseDataDTO searchFull(@RequestBody String text) {
        logger.info("validate provice : " + text);

        ResponseDataDTO res = new ResponseDataDTO();
        res = movieServiceImplement.searchFullText(text);
        return res;
    }

    @RequestMapping(value = "searchInvert", produces = {"application/json"}, method = RequestMethod.POST)
    public ResponseDataDTO searchInvert(@RequestBody String text) {
        logger.info("validate provice : " + text);

        ResponseDataDTO res = new ResponseDataDTO();
        res = movieServiceImplement.searchInvertIndex(text);
        return res;
    }

    @RequestMapping(value = "searchInvertDB", produces = {"application/json"}, method = RequestMethod.POST)
    public ResponseDataDTO searchInvertDB(@RequestBody String text) {
        logger.info("validate provice : " + text);

        ResponseDataDTO res = new ResponseDataDTO();
        res = movieServiceImplement.searchInvertIndexDB(text);
        return res;
    }
}
